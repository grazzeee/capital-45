(function($) {
    $(document).ready(function () {

        // ADD LOGO TO MENU
        $('<li class="menu-top menu-top-first menu-icon-menu" id="menu-menu"><a href="nav-menus.php" class="wp-first-item menu-top menu-top-first menu-icon-menu menu-top-first" aria-haspopup="false"><div class="wp-menu-arrow"><div></div></div><div class="wp-menu-image dashicons-before dashicons-menu"><br></div><div class="wp-menu-name">Menus</div></a></li>')
        .insertAfter('#adminmenu li:eq(0)');
        
        $('<li class="bae-home-link"><a target="_blank" href="https://codingcreed.co.uk/" class="site-logo"></a></li>')
        .insertBefore('#adminmenu li:eq(0)');

    
        // CLOSE SEO YOAST ON LOAD
        $('#wpseo-dashboard-overview').addClass('closed');

        // FIX YOAST ICON
        $('#yoast-ab-icon').css('background-image','unset');

        // DASHBOARD ADMIN PANNEL
        $('#bae-custom-panel').insertAfter('#welcome-panel');

        // DASHBOARD ADMIN PANNEL
        $('#bae-custom-welcome').insertAfter('#welcome-panel');

        // SHOW DASHBOARD FADE IN
        setTimeout(function(){
            $('#bae-custom-welcome').addClass('show');
            $('#bae-custom-panel').addClass('show');
            $('#dashboard-widgets-wrap').addClass('show');
        }, 500);

        // SET IFRAME HEIGHT
        if ($('.toplevel_page_video-tutorials')[0]) {
            page_height = $( '#adminmenu' ).height();            
            $('#iframe-container').css('height', page_height - 100 );
        }

    });
})(jQuery);