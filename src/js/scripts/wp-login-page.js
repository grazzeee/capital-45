$(document).ready(function(){
	
	if ( $(".login.wp-core-ui")[0] ){

		setTimeout(function() {
			$('.login.wp-core-ui').addClass('login-page-loaded');
		}, 500);

		setTimeout(function() {
			$('.login-bg').addClass('show');
		}, 1000);

		setTimeout(function() {
			$('#login').addClass('show');
		}, 2000);

		$('#login').children('h1').remove();

		$('.login-logo').prependTo('form');
		$('.social-links-wrap').appendTo('form');

		if ( $(".login-action-resetpass")[0] ){
			$('.login-logo').prependTo('#login');
			$('.social-links-wrap').appendTo('#login');
		}

		$('#backtoblog').remove();
		$('#nav').insertAfter('.forgetmenot');
	}

});