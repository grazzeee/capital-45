$(document).ready(function(){

	if ( $("#fullpage")[0] ){
		$('body').addClass('header-light');
		$('body').addClass('slider-present');
	} else {
		$('body').addClass('header-dark');
		$('body').addClass('slider-not-present');
	}

	new fullpage('#fullpage', {
		//options here
		licenseKey: '45B73B75-931E4142-A92D759B-D60F2ACD',

		scrollingSpeed: 600,
		autoScrolling: true,
		slidesNavigation: true,
		touchSensitivity: 15,
		cardsOptions: {perspective: 100, fadeContent: true, fadeBackground: true},
		
		onLeave: function(){	
			$('body').addClass('parallax-menu-hover');
			$('body').addClass('parallax-scrolling');

			$('#fp-nav').addClass('parallax-inMotion');
			$('body').removeClass('show-slide-title');

			setTimeout(function(){
				$('body').addClass('show-slide-title');
			}, 1000);

			setTimeout(function(){
				if ( $(".style-one.active")[0] || $(".style-three.active")[0] ){
					$('body').removeClass('header-light');
					$('body').removeClass('header-dark');
					$('body').addClass('header-light');
				}
	
				if ( $(".style-two.active")[0] ){
					$('body').removeClass('header-light');
					$('body').removeClass('header-dark');
					$('body').addClass('header-dark');
				}
			}, 500);

			$('body').removeClass('show-nav-menu');
		},
		
		afterLoad: function(){
			$('body').removeClass('parallax-menu-hover');
			$('#fp-nav').removeClass('parallax-inMotion');

			setTimeout(function(){
				$('body').removeClass('parallax-scrolling');
			}, 600);

			var getslide = $('.parallax-section.active').attr("data-anchor");
			if ( getslide != "section_1") {
				$('body').addClass('show-burger-menu');
			} else {
				$('body').removeClass('show-burger-menu');
			}
			console.log(getslide);
		}

	});
	
	$('#fp-nav').hover(
		function(){ 
			$('body').addClass('parallax-menu-hover');
			$('#fp-nav').addClass('fp-nav-menu-hover');
		},
		function(){ 
			$('body').removeClass('parallax-menu-hover');
			$('#fp-nav').removeClass('fp-nav-menu-hover');
		}
 	);

	$('body').addClass('show-slide-title');
});