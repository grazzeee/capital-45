$(document).ready(function(){
	$('.page-loading').addClass('start-loaded');

	setTimeout(function(){
		$('.page-loading').addClass('page-fully-loaded');
	}, 2000);

	setTimeout(function(){
		$('.page-loading').addClass('hide-page-loader');
	}, 2800);

	setTimeout(function(){
		$('.page-loading').addClass('remove-page-loader');
	}, 3500);
});