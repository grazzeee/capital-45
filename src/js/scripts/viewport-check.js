$(document).ready(function(){

	var checkView = $('.viewport_check:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');
		
		this.destroy();
	}, {
		offset: '90%'
	});

	var checkViewFast = $('.viewport_check_fast:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '100%'
	});

	var checkLeft = $('.viewport_check-left:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '90%'
	});

	var checkRight = $('.viewport_check-right:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '90%'
	});

	var checkUp = $('.viewport_check-up:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '90%'
	});

	var checkUp = $('.viewport_check-down:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '90%'
	});

	var calendarCheck = $('.evo_eventtop:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport');

		this.destroy();
	}, {
		offset: '90%'
	});

});