$(document).ready(function(){
	
	var $carousel = $('.navigation-popup-images').flickity({
		prevNextButtons: false,
		freeScroll: false,
		fade: false,
		wrapAround: true,
		pageDots: false,
		fullscreen: true,
		lazyLoad: 1,
		accessibility: false
	});

	// Flickity instance
	// var flkty = $carousel.data('flickity');

	$('.popup-nav-item').hover(
		function() { 
			var index = $(this).attr('data-slide-number');
			$carousel.flickity( 'select', index );

			$(".navigation-patterns").attr("data-hover", index);
		},

		function() {}
	);

});