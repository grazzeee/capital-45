$(document).ready(function(){

	// HAMBURRGER ON CLICK ADD CLASS TO BODY
	$('.hamburger').on( 'click', function() {

		if ($(".navigation-popup.is-active")[0]){
			$('body').css('overflow', 'unset');
			$('header').removeClass("menu-is-active");

			$(this).removeClass("is-active");
			$('.navigation-popup').removeClass("is-active");
			$('.logo-inner-wrap').removeClass('menu-is-active');
		} else {
			$('body').css('overflow', 'hidden');
			$('header').addClass("menu-is-active");

			$(this).addClass("is-active");
			$('.navigation-popup').addClass("is-active");
			$('.logo-inner-wrap').addClass('menu-is-active');
		}
	});

	// STICKY NAV
	var userScroll = $(document).scrollTop();
	$(window).on('scroll', function() {
		onScrollHeader();
	});

	if (!$("body.home")[0]){
		// Do something if class exists
		$('body').addClass('show-burger-menu');
	}

	function onScrollHeader() {
		var newScroll = $(document).scrollTop();
		   
	   	if ( newScroll > 114 || userScroll > 114 ) {
		  	$('body').addClass('user-has-scrolled');
	   	} else {
			$('body').removeClass('user-has-scrolled');
		}
		
		if (newScroll > 40 ) {
			$('body').addClass('show-scrolled-menu');
		} else {
			$('body').removeClass('show-scrolled-menu');
		}
	}
	onScrollHeader();
});