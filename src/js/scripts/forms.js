$(document).ready(function(){

    $('.wpforms-field').append('<div class="line"><span class="grey"></span><span class="black"></span></div>');


	// ON PAGE LOAD CHECK INPUT FIELDS ARE NOT EMPTY
	$('.wpforms-field-medium').each( function () {
		if ( this.value.trim() !== '' ) {
			$(this).parent().addClass('has_text');
		}
	});

	     
	// WHEN TEXT INPUT FIELD CLICKED HIGHLIGHT FIELD WITH ANIMATION
	$('.wpforms-field-medium').click(function() {
			$('.input_selected').removeClass('input_selected');
			$(this).parent().addClass('input_selected');
    }); 
    
});