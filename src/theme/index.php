<?php
	
	/**
	* Get header
	*/
	get_header();

	/**
	* Get flexible content and side bar
	*/
	include(get_stylesheet_directory() . "/template-parts/flexible-content/get-flexible-content.php");

	/**
	* Get footer
	*/
	get_footer();
?>