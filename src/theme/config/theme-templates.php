<?php

	/*
    * Hide page templates from clients
	*/
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

	if ( strpos($url,'bae.local' ) !== false || strpos($url,'baedemo' ) !== false ) {
		// Dev environment

	} else {
		add_filter( 'theme_page_templates', 'rrwd_remove_page_template' );
		function rrwd_remove_page_template( $pages_templates ) {
			unset( $pages_templates['index-header01.php'] );
			unset( $pages_templates['index-header02.php'] );
			unset( $pages_templates['index-header03.php'] );
			unset( $pages_templates['index-header04.php'] );
			unset( $pages_templates['index-header06.php'] );

			return $pages_templates;
		}
	}
	
?>