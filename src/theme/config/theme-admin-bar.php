<?php

	/*
    * The +New Post in Admin Bar
    */
	add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );
	function remove_default_post_type_menu_bar( $wp_admin_bar ) {
	    $wp_admin_bar->remove_node( 'new-post' );
	}

?>