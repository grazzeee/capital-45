<?php

	/*
    * Load style sheet on login pages
	*/
	function enque_loginPage_CSS() {
		wp_enqueue_style('login-styles', get_stylesheet_directory_uri() . '/assets/css/style.min.css');
		wp_enqueue_style('login-fonts', 'https://fonts.googleapis.com/css?family=Karla:400,400i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Montserrat:300,400,500,600,700&display=swap');
		wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/5.4.0/css/font-awesome.min.css'); 
	}
	add_action('login_enqueue_scripts', 'enque_loginPage_CSS');

	/*
    * Load scripts on login pages
	*/
	function enque_loginPage_scripts() {
		wp_deregister_script('jquery');

		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), filemtime(get_template_directory() . '/assets/js/jquery.min.js'), false);
		wp_enqueue_script('jquery');

		wp_register_script('conditionizr', get_template_directory_uri() . '/assets/js/conditionizr.js', array(), filemtime(get_template_directory() . '/assets/js/conditionizr.js'), false);
		wp_enqueue_script('conditionizr');

		wp_register_script('libraries', get_template_directory_uri() . '/assets/js/lib.min.js', array(), filemtime(get_template_directory() . '/assets/js/lib.min.js'), false);
		wp_enqueue_script('libraries');

		wp_register_script('loadscripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), filemtime(get_template_directory() . '/assets/js/scripts.min.js'), false);
		wp_enqueue_script('loadscripts');
	}
	add_action( 'login_enqueue_scripts', 'enque_loginPage_scripts' );

?>