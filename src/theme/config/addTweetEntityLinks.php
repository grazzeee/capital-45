<?php 
    function addTweetEntityLinks( $text ) {

        //Convert urls to <a> links
        $text = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $text);

        //Convert hashtags to twitter searches in <a> links
        $text = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $text);

        //Convert attags to twitter profiles in <a> links
        $text = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $text);

        return $text;
    }
?>