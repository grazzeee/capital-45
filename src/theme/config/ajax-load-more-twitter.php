<?php 
    
    add_action('wp_ajax_load_more_twitter_posts', 'load_more_twitter_posts_posts');
    add_action('wp_ajax_nopriv_load_more_twitter_posts', 'load_more_twitter_posts_posts');

    function load_more_twitter_posts_posts() {
        check_ajax_referer('load_more_twitter_posts', 'security');

        $twitter_username = $_POST['twitter_username'];

        $number_of_columns = $_POST['number_of_columns'];
        $number_of_rows = $_POST['number_of_rows'];
        $posts_to_show = $_POST['posts_to_show'];
        
        $currently_visible = $_POST['currently_visible'];
        $currently_returned = $currently_visible*4;

        $posts_to_show = $number_of_columns * $number_of_rows;
        $posts_to_return = $posts_to_show * 4 + $currently_returned;

        $get_twitter_posts = returnTweet($twitter_username, $posts_to_return );

        
        // echo 'twitter_username: '. $twitter_username.'<br />';

        // echo 'number_of_columns: '. $number_of_columns.'<br />';
        // echo 'number_of_rows: '. $number_of_rows.'<br />';
        // echo 'posts_to_show: '. $posts_to_show.'<br />';
        // echo 'currently_visible: '. $currently_visible.'<br />';
        // echo 'currently_returned: '. $currently_returned.'<br />';

        // echo 'posts_to_show: '. $posts_to_show.'<br />';
        // echo 'posts_to_return: '. $posts_to_return.'<br />';


        $tweet_counter = 1;
        $count = 1;

        if($get_twitter_posts) {
            foreach($get_twitter_posts as $tweet) {

                if ( $in_reply_to_status_id === null ) {

                    if ( $count > $currently_visible ) {

                        // CHECK IF POST IS IN REPLY TO
                        $in_reply_to_status_id = '';
                        $in_reply_to_status_id = $tweet['in_reply_to_status_id'];

                        if ( $tweet_counter <= $posts_to_show ) {
                            echo '<li class="tweet delay-tile-'. $tweet_counter .' ajax-loaded">';

                                echo '<div class="content-wrap viewport_check">';
                                                        
                                    //RETWEETED USER
                                    $retweet_name = $tweet['retweeted_status']['user']['name'];

                                    // Tweet
                                    $tweeted_by_id = $tweet['id'];
                                    $tweeted_by_screen_name = $tweet['user']['screen_name'];

                                    $tweeted_recount = $tweet['retweet_count'];
                                    $tweeted_favorite_count = $tweet['favorite_count'];

                                    if ( $retweet_name != '' ) {
                                        include(get_stylesheet_directory() . "/template-parts/parts/tweet-retweet-twitter-post.php");
                                    } else {
                                        include(get_stylesheet_directory() . "/template-parts/parts/tweet-twitter-post.php");
                                    }

                                    ?>
                                        <div class="tweet-social-share">
                                            <a class="social-share" data-link="social-popup" href="<?php echo 'https://twitter.com/intent/tweet?in_reply_to='. $tweeted_by_id .'&amp;related='. $tweeted_by_screen_name; ?>" target="Social Share">
                                                <div class="share-icon">
                                                    <svg id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m401.268 184.531c12.106-19.053 18.732-41.332 18.732-64.355 0-66.168-53.832-120-120-120h-180c-66.168 0-120 53.832-120 120v207.648l81.533-87.648h29.003c-11.733 18.535-18.536 40.487-18.536 64 0 66.168 53.832 120 120 120h218.467l81.533 87.648v-207.648c0-63.05-48.877-114.898-110.732-119.645zm-332.801 25.645-38.467 41.352v-131.352c0-49.626 40.374-90 90-90h180c49.626 0 90 40.374 90 90 0 24.109-9.683 47.135-26.732 64h-151.268c-28.128 0-54.023 9.732-74.505 26zm413.533 225.352-38.467-41.352h-231.533c-49.626 0-90-40.374-90-90s40.374-90 90-90h180c49.626 0 90 40.374 90 90z"/></svg>
                                                </div>
                                                <div class="share-text">Comment</div>
                                            </a>

                                            <a class="social-share" data-link="social-popup" href="<?php echo 'https://twitter.com/intent/retweet?tweet_id='. $tweeted_by_id .'&amp;related='. $tweeted_by_screen_name; ?>" target="Social Share">
                                                <div class="share-icon">
                                                    <svg width="18" height="18" viewBox="0 0 18 18"><path d="M17.712 11.961a.493.493 0 0 1 0 .698l-2.518 2.517a.491.491 0 0 1-.698 0l-2.517-2.517a.493.493 0 1 1 .698-.698l1.675 1.674V6.468c0-.817-.665-1.481-1.481-1.481H8.494a.494.494 0 1 1 0-.987h4.377a2.471 2.471 0 0 1 2.468 2.468v7.168l1.675-1.675a.493.493 0 0 1 .698 0zm-8.348 2.373a.494.494 0 0 1 0 .988H4.986a2.471 2.471 0 0 1-2.468-2.47V5.686L.843 7.36a.493.493 0 1 1-.698-.698l2.518-2.518a.493.493 0 0 1 .698 0l2.517 2.518a.493.493 0 1 1-.698.698L3.505 5.685v7.168c0 .817.665 1.48 1.48 1.48h4.379z"></path></svg>
                                                </div>
                                                <div class="share-text"><?php echo $tweeted_recount; ?></div>
                                            </a>

                                            <a class="social-share" data-link="social-popup" href="<?php echo 'https://twitter.com/intent/like?tweet_id='. $tweeted_by_id .'&related='. $tweeted_by_screen_name; ?>" target="Social Share">
                                                <div class="share-icon">
                                                    <svg width="18" height="18" viewBox="0 0 18 18">
                                                        <path d="M5.556 4.988c-1.368 0-2.569 1.309-2.569 2.801 0 3.778 4.63 6.632 5.628 6.673.998-.041 5.628-2.895 5.628-6.673 0-1.492-1.201-2.801-2.57-2.801-1.665 0-2.593 1.932-2.602 1.951-.152.371-.761.371-.913 0-.008-.019-.937-1.951-2.602-1.951zM2 7.789C2 5.771 3.662 4 5.556 4c1.508 0 2.522 1.041 3.059 1.797C9.152 5.041 10.165 4 11.673 4c1.895 0 3.557 1.771 3.557 3.789 0 4.197-4.906 7.629-6.606 7.661C6.906 15.418 2 11.986 2 7.789z"></path>
                                                    </svg>
                                                </div>
                                                <div class="share-text"><?php echo $tweeted_favorite_count; ?></div>
                                            </a>
                                        </div>
                                    <?php

                            echo '</div>';
                            
                            echo '</li>';
                            $tweet_counter++;
                        }
                    }
                    $count++;
                }
            }
        }

        wp_die();
    }
?>