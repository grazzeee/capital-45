<?php
    function getMetaData($scrape_url) {
        $content = file_get_contents( $scrape_url );

        $doc = new DOMDocument();
        $tags = array();

        // squelch HTML5 errors
        @$doc->loadHTML($content);
        
        $meta = $doc->getElementsByTagName('meta');
        foreach ($meta as $element) {
            
            $tag = [];
            foreach ($element->attributes as $node) {
                $tag[$node->name] = $node->value;
            }
            $tags []= $tag;
        }

        $previous_url = 'first value';
        $counter = 0; 

        foreach ($tags as $tag) {
           
            $url =  $tag['content'];

            if ( $previous_url !== $tag['content'] ) {
                if (
                    strpos($url, 'gif') !== false ||
                    strpos($url, 'jpg') !== false ||
                    strpos($url, 'jpeg') !== false ||
                    strpos($url, 'png') !== false ||
                    strpos($url, 'tiff') !== false ||
                    strpos($url, 'tif') !== false ) {

                    // $counter++;
                    if ( strpos($url, 'favicons') == false ) {

                        if ( $counter == 0) {

                            // Swap image url to small image
                            $image_url = $tag['content'];                            
                            $image_url = str_replace('large', 'small', $image_url);                            
                            $image_url = str_replace('medium', 'small', $image_url);                            

                            echo '<div class="tweet-img-wrap">';
                                echo '<a class="play-video-link" href="'. $scrape_url .'"></a>';
                                echo '<img src="'. $image_url .'" alt="twitter image">';
                            echo '</div>';

                            $previous_url = $image_url;
                        }

                        $counter++;
                    }
                }
            }

        }
    }
?>