<?php

    add_filter('acf/prepare_field/type=flexible_content', 'my_flexible_content_layouts');
    function my_flexible_content_layouts($field){
        
        // Bail early if no layouts
        if(!isset($field['layouts']) || empty($field['layouts']))
            return $field;
        
            foreach( $field['layouts'] as $layout_key => $layout ) {
            
                $hero = get_field('hero', 'option');
                    $get_hero_layouts = $hero['layouts'];
                        $hero_1 = $get_hero_layouts['hero_1'];
                        $hero_3 = $get_hero_layouts['hero_3'];
                        $hero_4 = $get_hero_layouts['hero_4'];
                        $hero_5 = $get_hero_layouts['hero_5'];

                // HERO
                if ( $hero_1 === true ) {
                    // Target layout name: hero-01
                    if( $layout['name'] === 'hero-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $hero_3 === true ) {
                    // Target layout name: hero-03
                    if( $layout['name'] === 'hero-03'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $hero_4 === true ) {
                    // Target layout name: hero-04
                    if( $layout['name'] === 'hero-04'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $hero_5 === true ) {
                    // Target layout name: hero-05
                    if( $layout['name'] === 'hero-05'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                $newsfeed = get_field('newsfeed', 'option');
                $get_newsfeed_layouts = $newsfeed['layouts'];
                    $newsfeed_pg_01 = $get_newsfeed_layouts['newsfeed_pg_01'];
                    $newsfeed_pg_02 = $get_newsfeed_layouts['newsfeed_pg_02'];
                    $newsfeed_caro_01 = $get_newsfeed_layouts['newsfeed_caro_01'];
                    $newsfeed_caro_02 = $get_newsfeed_layouts['newsfeed_caro_02'];
    
                // NEWSFEED
                if ( $newsfeed_pg_01 === true ) {
                    // Target layout name: newsfeed_pg_01
                    if( $layout['name'] === 'newsfeed-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $newsfeed_pg_02 === true ) {
                    // Target layout name: newsfeed_pg_02
                    if( $layout['name'] === 'newsfeed-pg-02'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $newsfeed_caro_01 === true ) {
                    // Target layout name: newsfeed_caro_01
                    if( $layout['name'] === 'newsfeed-caro-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $newsfeed_caro_02 === true ) {
                    // Target layout name: newsfeed_caro_01
                    if( $layout['name'] === 'newsfeed-caro-02'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }
                    
                $newsletter = get_field('newsletter', 'option');
                $get_newsletter_layouts = $newsletter['layouts'];
                    $newsletter_pg_01 = $get_newsletter_layouts['newsletter_pg_01'];
                
                // NEWSLETTER
                if ( $newsletter_pg_01 === true ) {
                    // Target layout name: newsletter_pg_01
                    if( $layout['name'] === 'newsletter-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }
        
         
                $schools = get_field('schools', 'option');
                $get_schools_layouts = $schools['layouts'];
                    $schools_pg_01 = $get_schools_layouts['schools_pg_01'];

                // SCHOOLS
                if ( $schools_pg_01 === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'schools-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                $staff = get_field('staff', 'option');
                $get_staff_layouts = $staff['layouts'];
                    $staff_pg_01 = $get_staff_layouts['staff_pg_01'];

                // SCHOOLS
                if ( $staff_pg_01 === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'staff-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }


                $policies = get_field('policies', 'option');
                $get_policies_layouts = $policies['layouts'];
                    $policies_pg_01 = $get_policies_layouts['policies_pg_01'];

                // SCHOOLS
                if ( $policies_pg_01 === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'policies-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                $vacancies = get_field('vacancies', 'option');
                $get_vacancies_layouts = $vacancies['layouts'];
                    $vacancies_pg_01 = $get_vacancies_layouts['vacancy-pg-01'];
                    $vacancies_pg_02 = $get_vacancies_layouts['vacancy-pg-02'];

                // VACANCIES
                if ( $vacancies_pg_01 === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'vacancy-pg-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                if ( $vacancies_pg_02 === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'vacancy-pg-02'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                $events = get_field('events-eventon', 'option');
                $get_events_layouts = $events['layouts'];
                    $calendar = $get_events_layouts['calendar'];

                // events
                if ( $calendar === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'calendar'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

                $email_subscribers = get_field('email_subscribers', 'option');
                $get_email_subscribers_layouts = $email_subscribers['layouts'];
                    $get_email_subscribers = $get_email_subscribers_layouts['cta-signup-01'];

                // email_subscribers
                if ( $get_email_subscribers === true ) {
                    // Target layout name: schools_pg_01
                    if( $layout['name'] === 'cta-signup-01'){
                    
                        // Check options and disable
                        unset( $field['layouts'][$layout_key] );
                    }
                }

            }
        
        // return
        return $field;

    }

    $newsfeed = get_field('newsfeed', 'option');
    $get_custom_post_types = $newsfeed['custom_post_types'];
    $cpt_newsfeeds = $get_custom_post_types['newsfeeds'];

    // echo 'cpt_newsfeeds: '. $cpt_newsfeeds .'<br />';
    if ( $cpt_newsfeeds === true ) {
        add_action( 'admin_init', 'remove_newsfeed_menu' );
        function remove_newsfeed_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_newsevents' );
        }
    }

    $newsletter = get_field('newsletter', 'option');
    $get_custom_post_types = $newsletter['custom_post_types'];
    $cpt_newsletters = $get_custom_post_types['newsletters'];
    // echo 'cpt_newsletters: '. $cpt_newsletters .'<br />';

    if ( $cpt_newsletters === true ) {
        add_action( 'admin_init', 'remove_newsletter_menu' );
        function remove_newsletter_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_newsletter' );
        }
    }

    $school = get_field('schools', 'option');
    $get_custom_post_types = $school['custom_post_types'];
    $cpt_schools = $get_custom_post_types['schools'];
    // echo 'cpt_schools: '. $cpt_schools .'<br />';

    if ( $cpt_schools === true ) {
        add_action( 'admin_init', 'remove_schools_menu' );
        function remove_schools_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_schools' );
        }
    }

    $staff = get_field('staff', 'option');
    $get_custom_post_types = $staff['custom_post_types'];
    $cpt_staff = $get_custom_post_types['staff'];
    // echo 'cpt_staff: '. $cpt_staff .'<br />';

    if ( $cpt_staff === true ) {
        add_action( 'admin_init', 'remove_staff_menu' );
        function remove_staff_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_staff' );
        }
    }

    $policies = get_field('policies', 'option');
    $get_custom_post_types = $policies['custom_post_types'];
    $cpt_policies = $get_custom_post_types['policies'];
    // echo 'cpt_policies: '. $cpt_policies .'<br />';

    if ( $cpt_policies === true ) {
        add_action( 'admin_init', 'remove_policies_menu' );
        function remove_policies_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_policies' );
        }
    }

    $vacancies = get_field('vacancies', 'option');
    $get_custom_post_types = $vacancies['custom_post_types'];
    $cpt_vacancies = $get_custom_post_types['vacancies'];

    if ( $cpt_vacancies === true ) {
        add_action( 'admin_init', 'remove_vacancies_menu' );
        function remove_vacancies_menu() {
            remove_menu_page( 'edit.php?post_type=cpt_careers' );
        }
    }

    $eventOn = get_field('events-eventon', 'option');
    $get_custom_post_types = $eventOn['custom_post_types'];
    $cpt_eventOn = $get_custom_post_types['eventon'];

    
    
    if ( $cpt_eventOn === true ) {
        add_action( 'admin_init', 'remove_eventOn_menu' );
        function remove_eventOn_menu() {
            remove_menu_page( 'edit.php?post_type=ajde_events' );
            remove_menu_page( 'eventon' );
        }
    }

    // $get_email_subscribers = get_field('events-eventon', 'option');
    // $get_custom_post_types = $get_email_subscribers['custom_post_types'];
    // $cpt_email_subscribers = $get_custom_post_types['email_subscribers'];

    // if ( $cpt_email_subscribers === true ) {
    //     add_action( 'admin_init', 'remove_email_subscribers' );
    //     function remove_email_subscribers() {
    //         remove_menu_page( 'es_dashboard' );
    //     }
    // }

?>