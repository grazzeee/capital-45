<?php

    /*
    * AFFILIATION
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=affiliation-caro-01', 'affiliation_caro_01', 10, 3);
    function affiliation_caro_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/affiliation-caro-01.jpg';
    }

    /*
    * CONTENT LAYOUTS
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=content-block', 'content_block', 10, 3);
    function content_block($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-full-width.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-two-columns', 'content_two_columns', 10, 3);
    function content_two_columns($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-two-columns.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-two-columns-custom-sidebar', 'content_two_columns_custom_sidebar', 10, 3);
    function content_two_columns_custom_sidebar($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-two-columns.jpg';
    }

    /*
    * CONTENT BLOCKS
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=content-accordion-01', 'content_accordion_01', 10, 3);
    function content_accordion_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-accordion-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-paragraph', 'content_paragraph', 10, 3);
    function content_paragraph($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-paragraph.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-table', 'content_table', 10, 3);
    function content_table($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-table.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-space', 'content_space', 10, 3);
    function content_space($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-full-width.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-caro-paragraph-01', 'content_caro_paragraph_01', 10, 3);
    function content_caro_paragraph_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-caro-paragraph-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-image', 'content_image', 10, 3);
    function content_image($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-image.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-image-page', 'content_image_page', 10, 3);
    function content_image_page($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-image.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-title-page-01', 'content_title_page_01', 10, 3);
    function content_title_page_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-title-page-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=content-title-01', 'content_title_01', 10, 3);
    function content_title_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/content-title.jpg';
    }

    
    



    /*
    * CTA
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-full-width', 'cta_full_width', 10, 3);
    function cta_full_width($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-full-width.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-img-01', 'cta_img_01', 10, 3);
    function cta_img_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-img-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-img-02', 'cta_img_02', 10, 3);
    function cta_img_02($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-img-02.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-btn-01', 'cta_btn_01', 10, 3);
    function cta_btn_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-btn-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-3icon-01', 'cta_3icon_01', 10, 3);
    function cta_3icon_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-3icon-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-4icon-coloured-01', 'cta_4icon_coloured_01', 10, 3);
    function cta_4icon_coloured_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-4icon-coloured-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-4icon-01', 'cta_4icon_01', 10, 3);
    function cta_4icon_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-4icon-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-4-circles', 'cta_4_circles', 10, 3);
    function cta_4_circles($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-4-circles.jpg';
    }
    
    add_filter('acfe/flexible/layout/thumbnail/layout=cta-caro-01', 'cta_caro_01', 10, 3);
    function cta_caro_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-caro-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-grid-01', 'cta_grid_01', 10, 3);
    function cta_grid_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-grid-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-link-01', 'cta_link_01', 10, 3);
    function cta_link_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-link-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=cta-signup-01', 'cta_signup_01', 10, 3);
    function cta_signup_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/cta-signup-01.jpg';
    }

    /*
    * CALENDAR
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=calendar', 'calendar', 10, 3);
    function calendar($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/calendar.jpg';
    }


    /*
    * VACANCY
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=vacancy-pg-01', 'vacancy_pg_01', 10, 3);
    function vacancy_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/vacancy-pg-01.jpg';
    }

    /*
    * CONTACT
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=contact-pg-01', 'contact_pg_01', 10, 3);
    function contact_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/contact-pg-01.jpg';
    }

    /*
    * GALLERY
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=gallery-pg-02', 'gallery_pg_02', 10, 3);
    function gallery_pg_02($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/gallery-pg-02.jpg';
    }

    
    /*
    * HERO
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=hero-01', 'hero_01', 10, 3);
    function hero_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/hero-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=hero-03', 'hero_03', 10, 3);
    function hero_03($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/hero-03.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=hero-04', 'hero_04', 10, 3);
    function hero_04($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/hero-04.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=hero-05', 'hero_05', 10, 3);
    function hero_05($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/hero-05.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=hero-nrw-01', 'hero_nrw_01', 10, 3);
    function hero_nrw_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/hero-nrw-01.jpg';
    }

    
    /*
    * LAYOUT
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=layout-two-columns', 'layout_two_columns', 10, 3);
    function layout_two_columns($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-two-columns.jpg';
    }
    
    add_filter('acfe/flexible/layout/thumbnail/layout=layout-full-width', 'layout_full_width', 10, 3);
    function layout_full_width($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/layout-full-width.jpg';
    }

        
    /*
    * NEWSFEED
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=newsfeed-pg-01', 'newsfeed_pg_01', 10, 3);
    function newsfeed_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/newsfeed-pg-01.jpg';
    }
    
    add_filter('acfe/flexible/layout/thumbnail/layout=newsfeed-pg-02', 'newsfeed_pg_02', 10, 3);
    function newsfeed_pg_02($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/newsfeed-pg-02.jpg';
    }
    

    add_filter('acfe/flexible/layout/thumbnail/layout=newsfeed-caro-01', 'newsfeed_caro_01', 10, 3);
    function newsfeed_caro_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/newsfeed-caro-01.jpg';
    }
    
    add_filter('acfe/flexible/layout/thumbnail/layout=newsfeed-caro-02', 'newsfeed_caro_02', 10, 3);
    function newsfeed_caro_02($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/newsfeed-caro-02.jpg';
    }


    /*
    * NEWSLETTER
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=newsletter-pg-01', 'newsletter_pg_01', 10, 3);
    function newsletter_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/newsletter-pg-01.jpg';
    }


    /*
    * POLICIES
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=policies-pg-01', 'policies_pg_01', 10, 3);
    function policies_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/policies-pg-01.jpg';
    }


    /*
    * STAFF
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=staff-pg-01', 'staff_pg_01', 10, 3);
    function staff_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/staff-pg-01.jpg';
    }


    /*
    * SCHOOLS
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=schools-pg-01', 'schools_pg_01', 10, 3);
    function schools_pg_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/schools-pg-01.jpg';
    }


    /*
    * SIDE BAR
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=side-bar-menu-01', 'side_bar_menu_01', 10, 3);
    add_filter('acfe/flexible/layout/thumbnail/layout=side-bar-menu-custom-01', 'side_bar_menu_01', 10, 3);
    function side_bar_menu_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/side-bar-menu-01.jpg';
    }
    

    /*
    * SOCAIL
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=social-twitter-01', 'social_twitter_01', 10, 3);
    function social_twitter_01($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/social-twitter-01.jpg';
    }

    add_filter('acfe/flexible/layout/thumbnail/layout=social-twitter-02', 'social_twitter_02', 10, 3);
    function social_twitter_02($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/social-twitter-02.jpg';
    }


    
    /*
    * UTILITY
    */

    add_filter('acfe/flexible/layout/thumbnail/layout=utility-page-forwarding', 'utility_page_forwarding', 10, 3);
    function utility_page_forwarding($thumbnail, $field, $layout){
        return get_template_directory_uri() . '/acf-image-select/modules/utility.jpg';
    }
?>
    