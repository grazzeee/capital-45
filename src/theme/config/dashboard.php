<?php 

    // Remove WP admin dashboard widgets
    function isa_disable_dashboard_widgets() {
        remove_meta_box('dashboard_right_now', 'dashboard', 'normal');// Remove "At a Glance"
        remove_meta_box('dashboard_activity', 'dashboard', 'normal');// Remove "Activity" which includes "Recent Comments"
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');// Remove Quick Draft
        remove_meta_box('dashboard_primary', 'dashboard', 'core');// Remove WordPress Events and News
    }
    add_action('admin_menu', 'isa_disable_dashboard_widgets');
    
    // Load custom dashbaord elements
    add_action( 'admin_footer', 'custom_dashboard_widget' );
    function custom_dashboard_widget() {

        // Bail if not viewing the main dashboard page
        if ( get_current_screen()->base !== 'dashboard' ) {
            return;
        }

        include( get_stylesheet_directory() . "/template-parts/admin/dashboard-quick-links.php" );
    }
    
    // SET DASHBOARD COLUMNS TO TWO
    function so_screen_layout_columns( $columns ) {
        $columns['dashboard'] = 2;
        return $columns;
    }
    add_filter( 'screen_layout_columns', 'so_screen_layout_columns' );

    function so_screen_layout_dashboard() {
        return 2;
    }
    add_filter( 'get_user_option_screen_layout_dashboard', 'so_screen_layout_dashboard' );
?>