<?php 

    add_action('wp_ajax_load_more_newsletter', 'load_more_newsletter_posts');
    add_action('wp_ajax_nopriv_load_more_newsletter', 'load_more_newsletter_posts');

    function load_more_newsletter_posts() {
        check_ajax_referer('load_more_newsletter', 'security');

            $paged = $_POST['page'];
            $args = array(
                'post_type' => 'cpt_newsletter',
                'post_status' => 'publish',
                'posts_per_page' => '4',
                'paged' => $paged,
            );

            $my_posts = new WP_Query( $args );
            
            if ( $my_posts->have_posts() ) :
                while ( $my_posts->have_posts() ) : $my_posts->the_post();

                    $postID = get_the_ID();
                    $media = get_field('media'); 
                    $image = $media['post_featured_image']['url'];
                    $title = get_the_title();
                    $preview = get_field('preview');
                    $file = get_field('file');

                    echo '<li class="newsletter-item ajax-load">';
                        include(get_stylesheet_directory() . "/template-parts/tiles/newsletter-tile-01.php");
                    echo '</li>';

                endwhile;
            endif;
     
        wp_die();
    }
?>