<?php

	/*
    * Add theme sidebar
    */
	function load_page_sidebar() {
		register_sidebar(
			array (
				'name' => __( 'Page Sidebar', 'page-sidebar' ),
				'id' => 'page-sidebar',
				'description' => __( 'All items here will display within the page sidebar', 'page-sidebar' ),
				'before_widget' => '<div class="widget-wrap">',
				'after_widget' => "</div>",
			)
		);
	}
	add_action( 'widgets_init', 'load_page_sidebar' );

?>