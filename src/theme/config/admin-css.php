<?php 
    add_action('admin_head', 'admin_custom_css');
    function admin_custom_css() {
        echo '<style>

            .bae-home-link {
                background-image: url("'. get_template_directory_uri() .'/assets/img/admin-logo-white.png");
            }
            
            #yoast-ab-icon {
                background-image: url("'. get_template_directory_uri() .'/assets/img/yoast-logo.png")!important;
            }

            #wpadminbar {
                background: #1e73be;
            }

            #wpadminbar .menupop .ab-sub-wrapper,#wpadminbar .shortlink-input {
                background: #1e73be;
            }
  
        </style>';
    }
?>