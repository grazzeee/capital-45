<?php

    add_action( 'login_init', 'wpse8170_login_init' );
    function wpse8170_login_init() {

        /**
        * Load Header
        */        
        echo '<div class="login-logo" style="display: none;">';
            echo '<img src="'. get_template_directory_uri() .'/assets/img/admin-logo.png" alt="site login logo">';
        echo '</div>';

        /**
        * Load Socials
        */
        include(get_stylesheet_directory() . "/template-parts/admin/social-links.php");

        /**
        * Load Socials
        */
        $image = get_field('login-bg-img', 'option');
        echo '<div class="login-bg" style="background-image: url('. aq_resize( $image['url'], 1920, 1200, true, true, true ) .')"></div>';
    }

?>