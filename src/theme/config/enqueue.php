<?php

    // Load header scripts
    function header_scripts() {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

            wp_deregister_script('jquery');

            wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), filemtime(get_template_directory() . '/assets/js/jquery.min.js'), false);
            wp_enqueue_script('jquery');

            wp_register_script('conditionizr', get_template_directory_uri() . '/assets/js/conditionizr.js', array(), filemtime(get_template_directory() . '/assets/js/conditionizr.js'), false);
            wp_enqueue_script('conditionizr');

            wp_register_script('libraries', get_template_directory_uri() . '/assets/js/lib.min.js', array(), filemtime(get_template_directory() . '/assets/js/lib.min.js'), false);
            wp_enqueue_script('libraries');

            wp_register_script('loadscripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), filemtime(get_template_directory() . '/assets/js/scripts.min.js'), false);
            wp_enqueue_script('loadscripts');
        }
    }

    add_action('init', 'header_scripts');


    // Load styles
    function styles() {
        
        wp_enqueue_style('plugin-styles', get_template_directory_uri() . '/assets/css/plugin-styles.min.css', array(), filemtime(get_template_directory() . '/assets/css/plugin-styles.min.css'), false);
        wp_enqueue_style('plugin-styles');

        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/style.min.css', array(), filemtime(get_template_directory() . '/assets/css/style.min.css'), false);
        wp_enqueue_style('styles');

        // wp_enqueue_style('icomoon', get_template_directory_uri() . '/assets/fonts/icomoon/style.css', array(), filemtime(get_template_directory() . '/assets/fonts/icomoon/style.css'), false);
        // wp_enqueue_style('icomoon');

        // wp_enqueue_style('font', 'https://use.typekit.net/jao8ztd.css', array(), false);
        // wp_enqueue_style('font');

        // wp_enqueue_style('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', false ); 
        // wp_enqueue_style('select2');

        wp_enqueue_style('googleFont', 'https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900|PT+Serif:400,400i,700,700i&display=swap', false );
        wp_enqueue_style('googleFont');
    }
    add_action('wp_enqueue_scripts', 'styles');



    // ADMIN SCRIPTS
    function enqueue_my_scripts() {
        wp_enqueue_script( 'handle', get_template_directory_uri() . '/assets/js/admin.min.js', array(), filemtime(get_template_directory() . '/assets/js/admin.min.js'), false);
    }
    add_action( 'admin_enqueue_scripts', 'enqueue_my_scripts' );

    // ADMIN STYLES
    function wpdocs_enqueue_custom_admin_style() {
        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/style-admin.min.css', array(), filemtime(get_template_directory() . '/assets/css/style-admin.min.css'), false);
        wp_enqueue_style( 'custom_wp_admin_css' );
    }
    add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );

?>