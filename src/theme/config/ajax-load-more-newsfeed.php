<?php 

    add_action('wp_ajax_load_more_newsfeed_archive_posts', 'load_more_newsfeed_archive_posts_posts');
    add_action('wp_ajax_nopriv_load_more_newsfeed_archive_posts', 'load_more_newsfeed_archive_posts_posts');

    function load_more_newsfeed_archive_posts_posts() {
        check_ajax_referer('load_more_newsfeed_archive_posts', 'security');
        
            $paged = $_POST['page'];
            $tile = $_POST['tile'];
            $posts = $_POST['posts'];
            $taxonomy = $_POST['taxonomy'];

            if ( $taxonomy == '' ) {
                $args = array(
                    'post_type' => 'cpt_newsevents',
                    'post_status' => 'publish',
                    'posts_per_page' => $posts,
                    'paged' => $paged,
                );
            } else {
                $args = array(
                    'post_type' => 'cpt_newsevents',
                    'cpt_tax_newsfeed_categories' => $taxonomy,
                    'post_status' => 'publish',
                    'posts_per_page' => $posts,
                    'paged' => $paged,
                );
            }

            $my_posts = new WP_Query( $args );
            $counter = 1;

            if ( $my_posts->have_posts() ) :
                while ( $my_posts->have_posts() ) : $my_posts->the_post();

                    $postID = get_the_ID();
                    $media = get_field('media'); 
                    $image = $media['post_featured_image']['url'];
                    $title = get_the_title();
                    $preview = get_field('preview');

                    echo '<li class="newsfeed-tile-0'. $tile .' ajax-load">';

                        // echo $paged;
                        // echo $tile;
                        // echo $posts;
                        // echo $taxonomy;

                        // include(get_stylesheet_directory() . "/template-parts/tiles/newsfeed-tile-02.php");
                        include(get_stylesheet_directory() . "/template-parts/tiles/newsfeed-tile-0". $tile .".php");
    
                    echo '</li>';

                    $counter++;
                endwhile;
            endif;
     
        wp_die();
    }

?>