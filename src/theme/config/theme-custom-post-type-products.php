<?php

	function cptui_register_my_cpts_cpt_products() {

		$cpt_products = get_field('products', 'option');
			$custom_post_types = $cpt_products['custom_post_types'];
			$rewrite_rule = $custom_post_types['rewrite_rule'];
		
		/**
		 * Post Type: Products.
		 */

		$labels = [
			"name" => __( "Products", "custom-post-type-ui" ),
			"singular_name" => __( "Course", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Products", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
            "rewrite" => [ "slug" => $rewrite_rule, "with_front" => false ],
			"query_var" => false,
			"menu_icon" => "dashicons-cart",
			"supports" => [ "title" ],
		];

		register_post_type( "cpt_products", $args );
	}

	add_action( 'init', 'cptui_register_my_cpts_cpt_products' );


    // DISABLE POST TYPE
    if (!function_exists('plugin_prefix_unregister_post_type_products')) {
        function plugin_prefix_unregister_post_type_products(){
            
			$cpt_products = get_field('products', 'option');
				$custom_post_types = $cpt_products['custom_post_types'];
					$on_or_off = $custom_post_types['products'];

            if ( $on_or_off == 1 ) {
                unregister_post_type( 'cpt_products' );
            }
        }
    }

    add_action('init','plugin_prefix_unregister_post_type_products');
?>