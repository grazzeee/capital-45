<?php

    /*
    * ENQUEUE SETTINGS
    */
    require_once(get_stylesheet_directory() . "/config/enqueue.php");
    require_once(get_stylesheet_directory() . "/config/enqueue-login-page.php");

    /*
    * CUSTOM ADMIN CSS STYLES
    */
    require_once(get_stylesheet_directory() . "/config/admin-css.php");

    /*
    * CUSTOM THEME SUPPORT
    */
    require_once(get_stylesheet_directory() . "/config/theme-admin-bar.php");
    require_once(get_stylesheet_directory() . "/config/theme-browser-support.php");
    require_once(get_stylesheet_directory() . "/config/theme-custom-post-type-products.php");
    require_once(get_stylesheet_directory() . "/config/theme-custom-post-types.php");
    require_once(get_stylesheet_directory() . "/config/theme-functions.php");
    require_once(get_stylesheet_directory() . "/config/theme-media.php");
    require_once(get_stylesheet_directory() . "/config/theme-menus.php");
    require_once(get_stylesheet_directory() . "/config/theme-pages.php");
    require_once(get_stylesheet_directory() . "/config/theme-security.php");
    require_once(get_stylesheet_directory() . "/config/theme-sidebars.php");
    require_once(get_stylesheet_directory() . "/config/theme-sitemap-fix.php");
    require_once(get_stylesheet_directory() . "/config/theme-utilities.php");
    require_once(get_stylesheet_directory() . "/config/theme-widgets.php");
    require_once(get_stylesheet_directory() . "/config/theme-wp-core.php");

    /*
    * CUSTOM THEME LOGIN
    */
    require_once(get_stylesheet_directory() . "/config/admin-login.php");

    /*
    * AJAX CALLS
    */
    require_once(get_stylesheet_directory() . "/config/ajax-load-more-newsfeed.php");
    require_once(get_stylesheet_directory() . "/config/ajax-load-more-newsletter.php");
    require_once(get_stylesheet_directory() . "/config/ajax-load-more-twitter.php");
    
    /*
    * ADVANCED CUSTOM FIELD SETTINGS
    */
    require_once(get_stylesheet_directory() . "/config/acf-settings.php");
    require_once(get_stylesheet_directory() . "/config/acf-layout-images.php");
    require_once(get_stylesheet_directory() . "/config/acf-layouts.php");

    /*
    * WP-ADMIN THEME SUPPORT
    */
    require_once(get_stylesheet_directory() . "/config/admin-menu.php");
    require_once(get_stylesheet_directory() . "/config/dashboard.php");

    /*
    * LOAD THIRD PARTY SCRIPTS
    */
    require_once(get_stylesheet_directory() . "/assets/scripts/aq_resizer.php");
    require_once(get_stylesheet_directory() . "/assets/scripts/aq_resizer_srcset.php");

    /*
    * THEME EXTENSIONS
    */
    require_once(get_stylesheet_directory() . "/config/addTweetEntityLinks.php");
    require_once(get_stylesheet_directory() . "/config/scrape-metadata.php");
    require_once(get_stylesheet_directory() . "/config/twitter-get-posts.php");
?>