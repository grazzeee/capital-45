<div class="background-images fill-container">
    <?php
        $ratio = '3840x2160';

        $size = array(
            array('w' => 200, 'upsize' => false),
            array('w' => 400),
            array('w' => 600),
            array('w' => 800),
            array('w' => 1000),
            array('w' => 1200),
            array('w' => 1400),
            array('w' => 1600),
            array('w' => 1800),
            array('w' => 2000),
            array('w' => 2400),
            array('w' => 2800),
            array('w' => 3200),
            array('w' => 3600),
            array('w' => 4000),
        );

        if( $image ):
            echo '<img class="desktop fill-container" src="'. aq_resize( $image, 1920, 1080, true, true, true ) .'" srcset="'. generate_srcset( $image, $size, $ratio) .'" alt="background image" />';
        endif;
    ?>
</div>