<?php

    $carousel_slider = get_sub_field('carousel_slider');
    
    $number_of_sections = 1;
    if($carousel_slider) {
        foreach($carousel_slider as $slide) {            
            $number_of_sections++;
        }
    }

    echo '<div class="page-boarder top"></div>';
    echo '<div class="page-boarder right"></div>';
    echo '<div class="page-boarder left"></div>';
    echo '<div class="page-boarder bottom"></div>';

    // NAVIGATION
    echo '<div id="fp-nav" class="fp-right" style="margin-top: -43.5px;">';
        echo '<div class="menu-border top"><span class="line"></span></div>';
        echo '<ul>';

            $section_count = 1;
            if($carousel_slider) {
                foreach($carousel_slider as $slide) {
                    $slide_type = $slide['slide_type'];

                    switch ($slide_type) {
                        case "style_one":

                            $style_one = $slide['style_one'];
                            $slide_title = $style_one['slide_title'];

                            break;
                        case "style_two":

                            $style_two = $slide['style_two'];
                            $slide_title = $style_two['slide_title'];

                            break;
                        case "style_three":

                            $style_three = $slide['style_three'];
                            $slide_title = $style_three['slide_title'];

                            break;
                    }
                    ?>
                        <li>
                            <a href="#section_<?php echo $section_count ?>" class=""></a>
                            <span class="square"></span>
                            <div class="slide-number">0<?php echo $section_count ?></div>
                            <div class="overlay"></div>
                        </li>
                    <?php

                    $section_count++;
                }
            }
        echo '</ul>';
        echo '<div class="menu-border bottom"><span class="line"></span></div>';
    echo '</div>';
    

    // SECTIONSs
    echo '<div id="fullpage">';
        $section_count = 1;
        if($carousel_slider) {
            foreach($carousel_slider as $slide) {
                
                $slide_type = $slide['slide_type'];

                switch ($slide_type) {
                    case "style_one":

                        include(get_stylesheet_directory() . "/template-parts/flexible-content-modules/parallax-slider-style-one.php");

                        break;
                    case "style_two":
 
                        include(get_stylesheet_directory() . "/template-parts/flexible-content-modules/parallax-slider-style-two.php");

                        break;
                    case "style_three":

                        include(get_stylesheet_directory() . "/template-parts/flexible-content-modules/parallax-slider-style-three.php");

                        break;
                }

                $section_count++;

            }
        }
    echo '</div>';
?>