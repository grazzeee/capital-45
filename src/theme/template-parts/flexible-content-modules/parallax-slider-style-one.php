<?php

    $style_one = $slide['style_one'];
    $slide_title = $style_one['slide_title'];
    $title = $style_one['title'];
    $image = $style_one['image']['url'];
    $video = $style_one['video_url'];

    // echo '<p>'. $style_one .'</p>';
    // echo '<p>'. $slide_title .'</p>';
    // echo '<p>'. $title .'</p>';
    // echo '<p>'. $image .'</p>';
    // echo '<pre>'. $image .'</pre>';

?>

<div data-anchor="section_<?php echo $section_count; ?>" class="section style-one parallax-section active">
    <div class="content-overlay">
        <div class="content-wrap">
            <div class="section-title">
                <h1><?php echo $title; ?></h1>
            </div>
        </div>
    </div>

    <div class="section-slide-title">
        <div class="line-wrap"><div class="line"></div></div>
        <div class="text-wrap"><span><?php echo $slide_title; ?></span></div>
    </div>

    <div class="background fill-container">
        <div class="background-fade fill-container"></div>

        <div class="patterns fill-container">
            <div class="puzzle-piece piece-1"></div> 
            <div class="puzzle-piece piece-2"></div> 
            <div class="puzzle-piece piece-3"></div> 
        </div>

        <?php if ( $video == null ) {
            include(get_stylesheet_directory() . "/template-parts/parts/background-image.php");
        } else { ?>
            <div class="vimeo-wrapper">
                <div class="navigation-popup-images-overlay"></div>
                <div class="navigation-popup-images-overlay-coloured"></div>
                <iframe src="<?php echo $video; ?>?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        <?php } ?>
    </div>
</div>