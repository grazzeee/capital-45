<?PHP //echo do_shortcode('[theme-my-login register_template="my-register-form.php"]'); ?>

<?php 
    $redirect_to = '';
    $image = get_field('login-bg-img', 'option');

?>

<div class="limiter">
    <div class="container-login100 viewport_check viewport_check-up">
        <div class="wrap-login100">

            <form class="login100-form validate-form" name="loginform" id="loginform" action="<?php echo site_url( '/wp-login.php' ); ?>" method="post">
                <div class="login-inner-wrap">

                    <div class="login-logo">
                        <img src="<?php echo get_template_directory_uri() .'/assets/img/admin-logo.png'; ?>" alt="">
                    </div>

                    <span class="login100-form-title p-b-43">Registrations are Closed.</span>
                    <span class="login100-form-title p-b-43">Log in to continue</span>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input  class="input100" type="text" name="log" id="user_login" class="input" value="" size="20" autocapitalize="off">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Username or Email Address</span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="pwd" id="user_pass" class="input" value="" size="20">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Password</span>
                    </div>

                    <div class="flex-sb-m w-full p-t-3 p-b-32">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100"  id="ckb1"  name="rememberme" type="checkbox" id="rememberme" value="forever">
                            <label class="label-checkbox100" for="ckb1">Remember me</label>
                        </div>

                        <div>
                            <a href="wp-login.php?action=lostpassword" class="txt1">Forgot Password?</a>
                        </div>
                    </div>


                    <!-- <input id="wp-submit" type="submit" value="Login" name="wp-submit"> -->
                    <input type="hidden" value="<?php echo esc_attr( $redirect_to ); ?>" name="redirect_to">
                    <input type="hidden" value="1" name="testcookie">
            
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">Login</button>
                    </div>
                    
                    <div class="text-center p-t-46 p-b-20">
                        <span class="txt2">Find us at</span>
                    </div>

                    <?php
                    	/**
                        * Get Socials
                        */
                        include(get_stylesheet_directory() . "/template-parts/admin/social-links.php");
                    ?>

                </div>
            </form>

            <!-- <div class="login100-more" style="background-image: url('');"> -->
            <img class="login-bg" src="<?php echo aq_resize( $image['url'], 1920, 1200, true, true, true ); ?>" alt="">
            
        </div>
    </div>
</div>