<?PHP //echo do_shortcode('[theme-my-login register_template="my-register-form.php"]'); ?>

<?php 
    $redirect_to = '';
    $image = get_field('login-bg-img', 'option');
?>

<div class="limiter">
    <div class="container-login100 viewport_check viewport_check-up">
        <div class="wrap-login100">

            <div class="login100-form validate-form" name="lostpasswordform" id="lostpasswordform">
                <div class="login-inner-wrap">

                    <div class="login-logo">
                        <img src="<?php echo get_template_directory_uri() .'/assets/img/admin-logo.png'; ?>" alt="">
                    </div>

                    <span class="login100-form-title p-b-43">Reset your password.</span>

                    <?php echo do_shortcode( '[theme-my-login resetpass show_links="0"]' ); ?>

                    <?php
                    	/**
                        * Get Socials
                        */
                        include(get_stylesheet_directory() . "/template-parts/admin/social-links.php");
                    ?>
                    
                </div>
            </div>

            <img class="login-bg" src="<?php echo aq_resize( $image['url'], 1920, 1200, true, true, true ); ?>" alt="">
            
        </div>
    </div>
</div>