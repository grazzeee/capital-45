<?PHP //echo do_shortcode('[theme-my-login register_template="my-register-form.php"]'); ?>

<?php 
    $redirect_to = '';
    $image = get_field('login-bg-img', 'option');
?>

<div class="limiter">
    <div class="container-login100 viewport_check viewport_check-up">
        <div class="wrap-login100">

            <form class="login100-form validate-form" name="lostpasswordform" id="lostpasswordform"  action="<?php echo wp_lostpassword_url(); ?>" method="post">
                <div class="login-inner-wrap">

                    <div class="login-logo">
                        <img src="<?php echo get_template_directory_uri() .'/assets/img/admin-logo.png'; ?>" alt="">
                    </div>

                    <span class="login100-form-title p-b-43">Reset your password.</span>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="input100 input" type="text" name="user_login" id="user_login" class="input" value="" size="20" autocapitalize="off" autocomplete="off" >
                        <span class="focus-input100"></span>
                        <span class="label-input100">Username or Email Address</span>
                    </div>
                    
                    <input type="hidden" name="redirect_to" value="">
                    <input style="display: none;" type="submit" name="wp-submit" id="wp-submit" class="" value="Get New Password">

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">Reset Password</button>
                    </div>

                    <?php
                    	/**
                        * Get Socials
                        */
                        include(get_stylesheet_directory() . "/template-parts/admin/social-links.php");
                    ?>
                    
                </div>
            </form>

            <img class="login-bg" src="<?php echo aq_resize( $image['url'], 1920, 1200, true, true, true ); ?>" alt="">
            
        </div>
    </div>
</div>