<?php 
    $welcome_message = get_field('welcome_message', 'option');
    $dashboard_links = get_field('dashboard_links', 'option');
?>

<div id="bae-custom-welcome" class="bae-dashboard">

    <div id="welcome-panel" class="welcome-panel">
        <div class="welcome-panel-content">

            <?php echo apply_filters('the_content', $welcome_message); ?>

            <div class="welcome-panel-column-container">
                <div class="welcome-panel-column">
                    <h3>Edit Pages</h3>
                    <ul>
                        <li><a href="/wp-admin/admin.php?page=nestedpages" class="welcome-icon welcome-edit-page">Edit a page</a></li>
                        <li><a href="/wp-admin/post-new.php?post_type=page" class="welcome-icon welcome-add-page">Add a new page</a></li>
                    </ul>
                </div>
                <div class="welcome-panel-column">
                    <h3>Edit Newsfeed</h3>
                    <ul>
                        <li><a href="/wp-admin/admin.php?page=nestedpages" class="welcome-icon welcome-edit-page">Edit a Newsfeed post</a></li>
                        <li><a href="/wp-admin/post-new.php?post_type=page" class="welcome-icon welcome-add-page">Add a Newsfeed post</a></li>
                    </ul>
                </div>
                <div class="welcome-panel-column welcome-panel-last">
                    <h3>Edit Careers</h3>
                    <ul>
                        <li><a href="/wp-admin/edit.php?post_type=cpt_careers" class="welcome-icon welcome-edit-page">Edit a vacancy</a></li>
                        <li><a href="/wp-admin/post-new.php?post_type=cpt_careers" class="welcome-icon welcome-add-page">Add a vacancy</a></li>
                    </ul>
                </div>

                <div class="welcome-panel-column">
                    <h3>Need some support?</h3>
                    <!-- <a class="button button-hero" href="https://en.support.wordpress.com/category/faq/">FAQ's</a>
                    <a class="button button-hero" href="https://www.wpbeginner.com/category/wp-tutorials/">Wordpress Tutorials</a> -->
                    <a class="button button-primary button-hero" href="https://www.blueappleeducation.com/">Contact Blue Apple</a>
                    <!-- <p class="support-ticket">or, <a href="http://localhost:8888/uw-website/wp-admin/themes.php">Open a support ticket with us.</a></p> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div id="bae-custom-panel" class="bae-dashboard">

    <?php 
        if( $dashboard_links ) {
            foreach( $dashboard_links as $slide ) {

                /**
                * Get Content
                */
                $title = $slide['title'];
                $logo = $slide['logo'];
                $link = $slide['link'];


                echo '<div id="ms-udb9720" class="postbox">';
                    echo '<a href="'. $link .'" target="_self"></a>';
                    echo '<h2 class="hndle ui-sortable-handle"><span>'. $title .'</span></h2>';
                    echo '<div class="inside">';
                        echo '<i class="dashicons '. $logo .'"></i>';
                    echo '</div>';
                echo '</div>';
            }
        }
    ?>


    <!-- <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/nav-menus.php" target="_self"></a>

        <h2 class="hndle ui-sortable-handle"><span>Menus</span></h2>
        <div class="inside">
            <i class="dashicons dashicons-menu"></i>
        </div>
    </div>

    <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/edit.php?post_type=ajde_events" target="_self"></a>
        <h2 class="hndle ui-sortable-handle"><span>Events</span></h2>
        <div class="inside">
            <i class="dashicons dashicons-calendar-alt"></i>
        </div>
    </div>

    <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/edit.php?post_type=cpt_staff" target="_self"></a>
        <h2 class="hndle ui-sortable-handle"><span>Staff Members</span></h2>
        <div class="inside">
            <i class="fa fa-user"></i>
        </div>
    </div>

    <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/edit.php?post_type=cpt_newsevents" target="_self"></a>
        <h2 class="hndle ui-sortable-handle"><span>Newsfeed</span></h2>
        <div class="inside">
            <i class="dashicons dashicons-admin-post"></i>
        </div>
    </div>

    <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/edit.php?post_type=cpt_newsletter" target="_self"></a>
        <h2 class="hndle ui-sortable-handle"><span>Newsletter</span></h2>
        <div class="inside">
        <i class="dashicons dashicons-admin-post"></i>
        </div>
    </div>

    <div id="ms-udb9720" class="postbox ">
        <a href="/wp-admin/edit.php?post_type=cpt_careers" target="_self"></a>
        <h2 class="hndle ui-sortable-handle"><span>Vacancies</span></h2>
        <div class="inside">
        <i class="dashicons dashicons-clipboard"></i>
        </div>
    </div> -->

</div>
