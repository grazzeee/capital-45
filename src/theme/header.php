<!DOCTYPE html>
<html>

	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		
		<?php wp_head(); ?>

	</head>

	<body <?php body_class( $bodyClasses ); ?>>

		<?php
			global $wp_query;  
			$post = $wp_query->post;
			$menu_style_on_load = get_field('menu_style_on_load', $post->ID); 

			//Navigation
			$popup_navigation = get_field('popup_navigation', 'option');
			$loading_image = get_field('404_background_image', 'option')['url'];
		?>

		<div class="page-loading">
			<div class="background"></div>
			<div class="logo-center">
				<svg width="49" height="71" viewBox="0 0 49 71" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity="0.8"><path class="aniamte" d="M49 54.6653C49 57.5477 48.354 60.1816 47.0122 62.5669C45.6704 64.9523 43.8813 66.7911 41.5953 68.1826C39.3093 69.5741 36.7252 70.2201 33.8428 70.2201C31.4574 70.2201 29.1217 69.7728 26.8854 68.8783C24.5994 67.9838 22.7606 66.8408 21.3195 65.4493L24.9473 60.1319C25.8915 61.2252 27.1339 62.07 28.6247 62.716C30.1653 63.3621 31.7059 63.71 33.2962 63.71C35.8306 63.71 37.9675 62.8651 39.6075 61.2252C41.2475 59.5852 42.0923 57.4483 42.0923 54.8641C42.0923 52.0314 41.2972 49.7454 39.7566 48.0061C38.216 46.2667 35.9797 45.3722 33.0974 45.3722C31.5568 45.3722 30.0659 45.571 28.6744 46.0183C27.2333 46.4655 25.7921 47.1116 24.2515 48.0061H24.2018V38.0172H0V32.7495L0.19878 32.4513L22.4625 0H28.7738L6.70893 32.7992H24.1521V23.8043H46.1177V30.3144H30.0162V40.3032C31.5071 39.9554 33.0477 39.7566 34.6876 39.7566C37.7191 39.7566 40.3032 40.4523 42.4402 41.7941C44.6268 43.1359 46.217 44.925 47.36 47.211C48.4534 49.3479 49 51.8824 49 54.6653Z" fill="white"/></g></svg>
			</div>

			<div style="background-image: url(<?php echo $loading_image ?>)" class="background-image"></div>
		</div>

		<header class="menu-style-<?php echo $menu_style_on_load; ?>">
			<div class="site-container">
				<div class="content-wrap">
					<div class="logo-outer-wrap viewport_check viewport_check-down">
						<a href="<?php echo get_site_url(); ?>" class="link">
							<div class="logo-inner-wrap">
								<div class="light">
									<svg width="49" height="71" viewBox="0 0 49 71" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity="0.8"><path d="M49 54.6653C49 57.5477 48.354 60.1816 47.0122 62.5669C45.6704 64.9523 43.8813 66.7911 41.5953 68.1826C39.3093 69.5741 36.7252 70.2201 33.8428 70.2201C31.4574 70.2201 29.1217 69.7728 26.8854 68.8783C24.5994 67.9838 22.7606 66.8408 21.3195 65.4493L24.9473 60.1319C25.8915 61.2252 27.1339 62.07 28.6247 62.716C30.1653 63.3621 31.7059 63.71 33.2962 63.71C35.8306 63.71 37.9675 62.8651 39.6075 61.2252C41.2475 59.5852 42.0923 57.4483 42.0923 54.8641C42.0923 52.0314 41.2972 49.7454 39.7566 48.0061C38.216 46.2667 35.9797 45.3722 33.0974 45.3722C31.5568 45.3722 30.0659 45.571 28.6744 46.0183C27.2333 46.4655 25.7921 47.1116 24.2515 48.0061H24.2018V38.0172H0V32.7495L0.19878 32.4513L22.4625 0H28.7738L6.70893 32.7992H24.1521V23.8043H46.1177V30.3144H30.0162V40.3032C31.5071 39.9554 33.0477 39.7566 34.6876 39.7566C37.7191 39.7566 40.3032 40.4523 42.4402 41.7941C44.6268 43.1359 46.217 44.925 47.36 47.211C48.4534 49.3479 49 51.8824 49 54.6653Z" fill="white"/></g></svg>
								</div>
								<div class="dark">
									<svg width="49" height="71" viewBox="0 0 49 71" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M27.0711 68.4141L27.0676 68.4127C24.9937 67.6012 23.3126 66.5871 21.9742 65.3768L25.0078 60.9303C25.9437 61.8644 27.0984 62.5996 28.4259 63.1748L28.4259 63.1748L28.4314 63.1771C30.024 63.845 31.6302 64.21 33.2962 64.21C35.9505 64.21 38.2187 63.3211 39.9611 61.5787C41.7044 59.8354 42.5923 57.5667 42.5923 54.8641C42.5923 51.9335 41.7672 49.522 40.1309 47.6746C38.4746 45.8045 36.088 44.8722 33.0974 44.8722C31.5153 44.8722 29.9724 45.0762 28.5238 45.5415C27.2652 45.9323 26.0114 46.4691 24.7018 47.1797V38.0172V37.5172H24.2018H0.5V32.9009L0.611077 32.7342L0.614809 32.7287L22.7258 0.5H27.8349L6.29406 32.5201L5.76995 33.2992H6.70893H24.1521H24.6521V32.7992V24.3043H45.6177V29.8144H30.0162H29.5162V30.3144V40.3032V40.9333L30.1298 40.7902C31.5867 40.4502 33.0891 40.2566 34.6876 40.2566C37.6407 40.2566 40.1294 40.9336 42.1743 42.2176L42.1787 42.2203C44.2814 43.5106 45.8093 45.2275 46.9128 47.4346L46.9149 47.4387C47.9659 49.4929 48.5 51.9466 48.5 54.6653C48.5 57.4697 47.8724 60.0179 46.5764 62.3218C45.2767 64.6323 43.5483 66.4085 41.3354 67.7555C39.1388 69.0925 36.6473 69.7201 33.8428 69.7201C31.521 69.7201 29.2483 69.285 27.0711 68.4141Z" fill="#121212" stroke="black"/></svg>
								</div>
							</div>
						</a>
					</div>

					<div class="overlay-menu">
						<div class="hamburger hamburger--collapse">
							<div class="hamburger-box">
								<div class="hamburger-inner"></div>
							</div>
						</div>
					</div>

					<div class="main-menu viewport_check">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
						</div>
					</div>

				</div>
			</div>
		</header>

		<div class="navigation-popup">

			<div class="section-slide-title">
				<div class="line-wrap"><div class="line"></div></div>
				<div class="text-wrap"><span>info@capital45.com</span></div>
			</div>

			<div class="section-slide-copy">
				<div class="text-wrap"><span>Website by <a href="https://codingcreed.co.uk">Coding creed</a></span></div>
			</div>

			<div class="popup-boarder top"></div>
			<div class="popup-boarder right"></div>
			<div class="popup-boarder left"></div>
			<div class="popup-boarder bottom"></div>
			
			<div class="navigation-wrap">
				<ul class="navigation-popup-menu">
					<?php
						$counter = 1;

						if($popup_navigation) {
							foreach($popup_navigation as $slide) {

								$name = $slide['name'];
								$url = $slide['url']; ?>

								<li data-slide-number="<?php echo $counter; ?>" class="popup-nav-item">
									<a href="<?php echo $url; ?>">
										<span class="slide-title font"><?php echo $name ?></span>
									</a>
								</li> <?php

								$counter++;
							}
						}
					?>
				</ul>
			</div>

			<div class="navigation-popup-images-overlay"></div>

			<div data-hover="0" class="navigation-patterns">
				<div class="patterns fill-container">
					<div class="puzzle-piece piece-1"></div> 
				</div>
			</div>

			<div class="navigation-popup-images">

				<?php
					if($popup_navigation) {
						foreach($popup_navigation as $slide) {

							/**
							* Set image resolutions
							*/
							$desktopRatio = '3840x2160';
							$tabletRatio = '1668x2224';
							$mobileRatio = '828x1792';
		
							/**
							* Get images
							*/
							$image = $slide['image']['url']; ?>

							<div class="slide-container">
								<div class="background-container">
									<?php include(get_stylesheet_directory() . "/template-parts/parts/background-image.php"); ?>
								</div>
							</div> <?php

							$counter++;
						}
					}
				?>
			</div>

		</div>


		<!-- OPEN MAIN -->
		<main class="main">