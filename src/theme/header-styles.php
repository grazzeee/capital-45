<?php 

    /*------------------------------------*\
	 BRANDING COLOURS
    \*------------------------------------*/

    $branding_colours = get_field('branding_colours', 'option');
    $primary = $branding_colours['primary'];
    $secondary = $branding_colours['secondary'];
    $tertiary = $branding_colours['tertiary'];
    $quaternary = $branding_colours['quaternary'];

    /*------------------------------------*\
	 BUTTON COLOURS
    \*------------------------------------*/

    $button_colours = get_field('button_colours', 'option');
    $bt_primary = $button_colours['primary'];
    $bt_secondary = $button_colours['secondary'];
    $bt_tertiary = $button_colours['tertiary'];
    $bt_quaternary = $button_colours['quaternary'];

    /*------------------------------------*\
	 HEADER COLOURS
    \*------------------------------------*/

    $header_colours = get_field('header_colours', 'option');
    $header_background_colour = $header_colours['header_background_colour'];

    if ( $header_background_colour == '' ) {
        $header_background_colour = '#ffffff';
    }

    /*------------------------------------*\
	 FOOTER COLOURS
    \*------------------------------------*/

    $footer_colours = get_field('footer_colours', 'option');

    $main_section_bg_colour = $footer_colours['main_section_bg_colour'];
    if ( $main_section_bg_colour == '' ) {
        $main_section_bg_colour = '#315d98';
    }

    $copyright_section_bg_colour = $footer_colours['copyright_section_bg_colour'];
    if ( $copyright_section_bg_colour == '' ) {
        $copyright_section_bg_colour = '#2a4f80';
    }

    $copyright_text_colour = $footer_colours['copyright_text_colour'];
    if ( $copyright_text_colour == '' ) {
        $copyright_text_colour = '#5f83b4';
    }

    $copyright_text_hover_colour = $footer_colours['copyright_text_hover_colour'];
    if ( $copyright_text_hover_colour == '' ) {
        $copyright_text_hover_colour = '#5f83b4';
    }

    /*------------------------------------*\
	 MODULE COLOURS
    \*------------------------------------*/
    $module_colours = get_field('module_colours', 'option');

    $social_twitter_01 = $module_colours['social-twitter-01'];
    $social_twitter_01_background = $social_twitter_01['background_colours_basic'];


    $cta_3icon_01 = $module_colours['cta-3icon-01'];
    $cta_3icon_01_background = $cta_3icon_01['background_colours_basic'];

?>

<style>

    /*------------------------------------*\
	 HEADER COLOURS
    \*------------------------------------*/

    .show-scrolled-menu header,
    .force-header-visible header {
            background-color: <?php echo $header_background_colour; ?>
    }

    /*------------------------------------*\
	 FOOTER COLOURS
    \*------------------------------------*/

    .footer01,
    .footer02,
    .footer03 {
        background-color: <?php echo $main_section_bg_colour; ?>;
    }

    .footer01 .lower-block,
    .footer02 .lower-block,
    .footer03 .lower-block {
        background-color: <?php echo $copyright_section_bg_colour; ?>;
    }

    .footer01 .lower-block a,
    .footer01 .lower-block span,
    .footer02 .lower-block a,
    .footer02 .lower-block span,
    .footer03 .lower-block a,
    .footer03 .lower-block span {
        color: <?php echo $copyright_text_colour; ?>;
    }

    .footer01 .lower-block a:hover,
    .footer02 .lower-block a:hover,
    .footer03 .lower-block a:hover {
        color: <?php echo $copyright_text_hover_colour; ?>;
    }


    /*------------------------------------*\
	 BUTTON COLOURS
    \*------------------------------------*/

    /* DEFAULT COLOUR */
    .button--ujarak {
        border-color: <?php echo $bt_primary; ?>;
        background-color: <?php echo $bt_primary; ?>;
    }

    .button--ujarak:hover {
        border-color: <?php echo $bt_primary; ?>;
    }

    .button--ujarak:hover span {
        color: <?php echo $bt_primary; ?>;        
    }

    /* bt_PRIMARY */
    .button--ujarak.bt-primary {
        background-color: <?php echo $bt_primary; ?>;
        border-color: <?php echo $bt_primary; ?>;
    }

    .button--ujarak.bt-primary:hover {
        border-color: <?php echo $bt_primary; ?>;
    }

    .button--ujarak.bt-primary:hover span {
        color: <?php echo $bt_primary; ?>;
        border-color: <?php echo $bt_primary; ?>;
    }

    /* SECONDARY */
    .button--ujarak.bt-secondary {
        background-color: <?php echo $bt_secondary; ?>;
        border-color: <?php echo $bt_secondary; ?>;
    }

    .button--ujarak.bt-secondary:hover {
        border-color: <?php echo $bt_secondary; ?>;
    }

    .button--ujarak.bt-secondary:hover span {
        color: <?php echo $bt_secondary; ?>;
        border-color: <?php echo $bt_secondary; ?>;
    }

    /* TERTIARY */
    .button--ujarak.bt-tertiary {
        background-color: <?php echo $bt_tertiary; ?>;
        border-color: <?php echo $bt_tertiary; ?>;
    }

    .button--ujarak.bt-tertiary:hover {
        border-color: <?php echo $bt_tertiary; ?>;
    }

    .button--ujarak.bt-tertiary:hover span {
        color: <?php echo $bt_tertiary; ?>;
        border-color: <?php echo $bt_tertiary; ?>;
    }

    /* QUATERNARY */
    .button--ujarak.bt-quaternary {
        background-color: <?php echo $bt_quaternary; ?>;
        border-color: <?php echo $bt_quaternary; ?>;
    }

    .button--ujarak.bt-quaternary:hover {
        border-color: <?php echo $bt_quaternary; ?>;
    }

    .button--ujarak.bt-quaternary:hover span {
        color: <?php echo $bt_quaternary; ?>;
        border-color: <?php echo $bt_quaternary; ?>;
    }


    /*------------------------------------*\
	 SINGLE POST COLOURS
    \*------------------------------------*/

    .row-post-categories-list ul li.cat-title,
    .row-single-recent-posts-list .recent-title ul li.cat-title {
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }

    .row-post-categories-list ul li a:hover {
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }

    .row-post-categories-list ul li.taxonomy-active a {
        background-color: <?php echo $bt_primary; ?>;
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }


    /*------------------------------------*\
	 SIDE BAR MENU
    \*------------------------------------*/

    .row-side-bar-menu-01 .side-menu .side-menu-header,
    .row-side-bar-menu-custom-01 .side-menu-custom .side-menu-header {
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }

    .row-side-bar-menu-01 .side-menu ul li a:hover,
    .row-side-bar-menu-custom-01 .side-menu-custom ul li a:hover {
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }

    .row-side-bar-menu-01 .side-menu ul li.page-active a, 
    .row-side-bar-menu-01 .side-menu ul li.current_page_item a,
    .row-side-bar-menu-custom-01 .side-menu-custom ul li.page-active a,
    .row-side-bar-menu-custom-01 .side-menu-custom ul li.current_page_item a {
        background-color: <?php echo $bt_primary; ?>;
        border-bottom: 2px solid <?php echo $bt_primary; ?>;
    }


    /*------------------------------------*\
	 CALENADR COLOURS
    \*------------------------------------*/

    .eventon_events_list .eventon_list_event:nth-child(4n-7) {
        background-color: <?php echo $primary; ?>!important;
    }

    .eventon_events_list .eventon_list_event:nth-child(4n-6) {
        background-color: <?php echo $secondary; ?>!important;
    }
        
    .eventon_events_list .eventon_list_event:nth-child(4n-5) {
        background-color: <?php echo $tertiary; ?>!important;
    }
        
    .eventon_events_list .eventon_list_event:nth-child(4n-4) {
        background-color: <?php echo $quaternary; ?>!important;
    }

    .evo_lightboxes .evcal_evdata_cell p a {
        color: <?php echo $primary; ?>!important;
    }


    /*------------------------------------*\
	 NEWS FEED TILE 02 COLOURS
    \*------------------------------------*/

    .newsfeed-tile-02:nth-child(4n+1) .content-wrap .color-block {
        background-color: <?php echo $primary; ?>;
    }

    .newsfeed-tile-02:nth-child(4n+2) .content-wrap .color-block {
        background-color: <?php echo $secondary; ?>;
    }

    .newsfeed-tile-02:nth-child(4n+3) .content-wrap .color-block {
        background-color: <?php echo $tertiary; ?>;
    }

    .newsfeed-tile-02:nth-child(4n+4) .content-wrap .color-block {
        background-color: <?php echo $quaternary; ?>;
    }

    /*------------------------------------*\
	 MODULE COLOURS
    \*------------------------------------*/
    
    .row-social-twitter-01 {
        background-color: <?php echo $social_twitter_01_background; ?>;
    }

    .row-social-twitter-02 .tweet .content-wrap a {
        color: <?php echo $bt_primary; ?>!important;
    }

    .row-social-twitter-02 .tweet .tweet-author-name a {
        color: #000!important;
    }

    .row-cta-3icon-01 {
        background-color: <?php echo $cta_3icon_01_background; ?>;
    }

    /*------------------------------------*\
	 MENU COLOURS
    \*------------------------------------*/

    .wp-nav-wrap .menu-main-menu-container ul.menu li.menu-item ul.sub-menu li.menu-item::before {
        background-color: <?php echo $bt_primary; ?>;
    }

    /*------------------------------------*\
	 COOKIE BAR COLOURS
    \*------------------------------------*/

    #cookie-law-info-bar {
        background-color: <?php echo $bt_primary; ?>!important;
    }


    /*------------------------------------*\
	 CAROUSEL PARAGRAPH
    \*------------------------------------*/
    .row-cta-caro-01 .content-wrap a {
        color: <?php echo $bt_primary; ?>!important;
    }

    /*------------------------------------*\
	 SEARCH RESULTS COLOURS
    \*------------------------------------*/

    .search-results-wrap-item a.page-title {
        color: <?php echo $bt_primary; ?>;
    }

    /*------------------------------------*\
	 VACANCIES 02 MODULE
    \*------------------------------------*/
    .row-vacancy-pg-02 .vacancies-tiles-wrap .vacancy-tile-wrap .vacancy-content-wrap .preview-wrap .preview {
        color: <?php echo $bt_primary; ?>;
    }


    .row-vacancy-pg-02 .vacancies-tiles-wrap .vacancy-tile-wrap .vacancy-content-wrap .location-wrap svg path {
        fill: <?php echo $bt_primary; ?>!important;
    }

    /*------------------------------------*\
	 FORM SUBMISSION
    \*------------------------------------*/
    .wpcf7-response-output.wpcf7-mail-sent-ok {
        background-color: <?php echo $bt_primary; ?>!important;
        color: #fff!important;
    }

    /*------------------------------------*\
	 PAGE CONTENT
    \*------------------------------------*/
    
    .row-content-paragraph .content-paragraph-wrap .title {
        color: <?php echo $bt_primary; ?>!important;
    }

    .row-content-paragraph .content-paragraph-wrap a {
        color: <?php echo $bt_primary; ?>!important;
    }

    /*------------------------------------*\
	 CONTACT US
    \*------------------------------------*/
    .row-contact-pg-01 .column.sidebar .splitter .splitter-line {
        background-color: <?php echo $bt_primary; ?>!important;
    }
}

</style>