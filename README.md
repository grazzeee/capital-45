Getting Started With this Wordpress Theme
===================================

This theme uses gulp for the build processes and Node Package Manager with Bower for all the package management. 
The theme is built with Advanced Custom Fields. It makes use of the Flexible Content blfield within ACF to build the site within a modular approach.

## Some links for more in depth learning
### Hands on / interactive learning
* [Advance Custom Fields](https://www.advancedcustomfields.com) Advanced custom fields website contains resources on how to use its fields. 
* [Advance Custom Fields - Flexible Content](https://www.advancedcustomfields.com/resources/flexible-content/) Flexible Content is used to manage the modules that build up the pages. 




Theme setup
==================

Instructions on how to get the theme working locally. 

- Pull down the repository
        
        $ git pull  

- Navigate to the repository root
        
        $ npm install "this wil pull down all the packages needed to build the them such as npm, bower & gulp"

        $ npm run bower install  "this will download all the bower packages listed in the bower file such as wordpress and some third party plugins" 

        $ gulp b1uild "this will build the theme with all the files pulled down"
    
        $ gulp watch " this will start watching for changes when working on the source files





Installing theme assets
==================

## Images

We need to add images to the correct location.

            /wp-content/uploads/


## Database

Remember to add the database to your local web server.


## Load Advanced Custom Fields data 

When building locally we need the ACF fields added to the theme so we can make amends to fields if needed.  ACF exports the field data to a json and php folder located within the themes root directly. When the theme is hosted on the server ACF will pull field data from the theme rather than the database. To ensure this is working correctly remove the ACF field data from within ACF. 




Folder structure
==================

- Bower_components
bower components are saved here when downloaded

- Dist
this is where gulp installs wordpress and the theme files

- node_modules
node packages are saved here when downloaded

- src
this is where the source files are kept and where you should be editing them.

- Wordpress_setup
this is where core wordpress files are saved to.



Working within the theme
==================

- Work from the ‘SRC’ folder as gulp watch moves all the source files into dist. The watch processes builds the theme every time it picks up a changes. All changes in dist will be overwritten. 

- If for whatever reason the files have not updated or moved into the /dist folder just start the watch task.


SRC Folder
==================

- /css - 3rd party css files can be added here. Gulp will minify these into one file and add them to. Most of the files here come from npm packages. 
            /dist/wp-content/themes/theme_name/assets/css/plugin-styles.min.css

- /js - is split into 3 folders

- - /admin - admin scripts located here. These load in the Wordpress admin area only. On watch these get minified to:
            /dist/wp-content/themes/theme_name/assets/js/admin.min.js

- - /lib - is where 3rd party libraries and packages are saved to. On watch these get minified to: 
            /dist/wp-content/themes/theme_name/assets/js/lib.min.js

- - /scripts - is where all the site scripts are saved to. On watch these get minified to:
            /dist/wp-content/themes/theme_name/assets/js/scripts.min.js

- /sass - All the site styles are saved here. They are minified on watch and saved to:
            /dist/wp-content/themes/theme_name/assets/css/style.min.css

- - /sass-admin - All the site admin styles are saved here. They are minified on watch and saved to:
            /dist/wp-content/themes/theme_name/assets/css/style-admin.min.css

- /theme - this is the themes core files. They are edited here and moved to the following location on save.
            /dist/wp-content/themes/theme_name

- /theme-plugins - that are not available via bower are saved here and moved to the correct location on the gulp build process.





Making amends to Advanced Custom Fields (ACF)
==================

## Import ACF fields in to wordpress. 

From Wordpress admin dashboard navigate to Custom FIelds => Tools.   Select the Json file you wish to import and import and click the upload button. Once uploaded you should see the uploaded fields appear within the field groups tab under Custom Fields.


## ACF Flexible content

ACF Flexible content fields have been used to manage the field groups. ACF Flexible content allows for the page to have a drag and drop modular approach. [Advance Custom Fields - Flexible Content](https://www.advancedcustomfields.com/resources/flexible-content/)

- **flexible content** is where you can find all the main page modules.
- **Flexible Modules Content** is where you can find all the page content modules.


## Saving ACF Fields

When saving the field groups ACF exports the fields to a readable file. The theme on the live server reads these files and loads the fields from the theme.

- PHP
            /dist/wp-content/themes/theme_name/acfe-php
- JSON
            /dist/wp-content/themes/theme_name/acfe-php


## Flexible Content Files

When adding a new layout to the flexible content field group located in the ‘custom fields’ => field groups tab you will need to enter some information. 

**Label** - name of the module
**Name** - name of the file used to display the module fields
**Layout** - choose the layout of the fields
**Category** - category the module will be added to.

You can find the module files here
            /github repo/src/theme/template-parts/flexible-content